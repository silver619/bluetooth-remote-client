package com.hamdan.bluetoothremote;

public interface Constants {
    // BluetoothService Handler Messages
    int MESSAGE_READ = 0;
    int MESSAGE_WRITE = 1;
    int MESSAGE_TOAST = 2;
    int MESSAGE_CONNECTED = 3;

    // key names
    String TOAST = "toast";
    String CLOSE_ACTIVITY = "close_activity";
    String DEVICE_EXTRA = "device_extra";

    // Commands
    String SINGLE_LEFT_CLICK_COMMAND = "l_click_single";
    String LEFT_CLICK_DOWN_COMMAND = "l_click_down";
    String LEFT_CLICK_UP_COMMAND = "l_click_up";
    String RIGHT_CLICK_DOWN_COMMAND = "r_click_down";
    String RIGHT_CLICK_UP_COMMAND = "r_click_up";
    String BACKSPACE_COMMAND = "backspace";
    String KEYBOARD_COMMAND_PREFIX = "keyx_x";

    String QUIT_SLIDESOW_COMMAND = "quit";
    String STOP_COMMAND = "stop";
    String BACKWARD_COMMAND = "back";
    String FORWARD_COMMAND = "forward";
    String RESTART_SLIDESHOW_COMMAND = "restart_slide";

    String POWER_COMMAND = "power";
    String RESTART_COMMAND = "restart";
    String LOGOUT_COMMAND = "logoff";
    String HIBERNATE_COMMAND = "hibernate";
}
