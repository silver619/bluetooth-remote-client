package com.hamdan.bluetoothremote;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hamdan.bluetoothremote.ui.devices.DeviceListItem;
import com.hamdan.bluetoothremote.ui.devices.DevicesListAdapter;

import java.util.ArrayList;
import java.util.Set;

/**
 * Activity responsible for listing paired devices to user and also allowing user to discover new
 * devices to connect with
 */
public class DevicesActivity extends AppCompatActivity {
    private static final String TAG = "DevicesActivity";

    public ArrayList<DeviceListItem> pairedDevicesList = new ArrayList<>();
    public ArrayList<DeviceListItem> discoveredDevicesList = new ArrayList<>();
    private DevicesListAdapter pairedDevicesAdapter;
    private DevicesListAdapter discoveredDevicesAdapter;

    private BluetoothAdapter bluetoothAdapter;
    private Button findButton;
    private ProgressBar discoveryProgress;
    private SwipeRefreshLayout pairedSwipeRefresh;
    private TextView pairedEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);

        discoveryProgress = findViewById(R.id.discoveryProgressBar);
        discoveryProgress.setVisibility(View.GONE);

        pairedSwipeRefresh = findViewById(R.id.pairedSwipeRefresh);
        pairedSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPairedDevices();
            }
        });

        pairedEmpty = findViewById(R.id.pairedEmpty);
        pairedEmpty.setVisibility(View.GONE);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        ListView pairedDevicesListView = findViewById(R.id.pairedList);
        pairedDevicesAdapter = new DevicesListAdapter(this, pairedDevicesList);
        pairedDevicesListView.setAdapter(pairedDevicesAdapter);
        pairedDevicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent controlIntent = new Intent(view.getContext(), ControlActivity.class);
                controlIntent.putExtra(Constants.DEVICE_EXTRA, pairedDevicesList.get(i).getDevice());
                startActivity(controlIntent);
            }
        });

        ListView discoveredDevicesListView = findViewById(R.id.discoveredList);
        discoveredDevicesAdapter = new DevicesListAdapter(this, discoveredDevicesList);
        discoveredDevicesListView.setAdapter(discoveredDevicesAdapter);
        discoveredDevicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent controlIntent = new Intent(view.getContext(), ControlActivity.class);
                controlIntent.putExtra(Constants.DEVICE_EXTRA, discoveredDevicesList.get(i).getDevice());
                startActivity(controlIntent);
            }
        });
        refreshPairedDevices();

        findButton = findViewById(R.id.findButton);
        findButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performDiscovery();
            }
        });

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(receiver, filter);

        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(receiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshPairedDevices();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // stop bluetooth device discovery when closing activity
        if (bluetoothAdapter != null) {
            bluetoothAdapter.cancelDiscovery();
        }
        unregisterReceiver(receiver);
    }

    // Method that handles the bluetooth device discovery intent
    protected void performDiscovery() {
        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
            findButton.setText(R.string.find_devices);
            discoveryProgress.setVisibility(View.GONE);
        } else {
            if (bluetoothAdapter.isEnabled()) {
                discoveredDevicesList.clear();
                discoveredDevicesAdapter.notifyDataSetChanged();
                findButton.setText(R.string.finding_devices);
                discoveryProgress.setVisibility(View.VISIBLE);
                bluetoothAdapter.startDiscovery();
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, MainActivity.REQUEST_ENABLE_BT);
            }
        }
    }

    // Method responsible for refreshing the paired devices list
    protected void refreshPairedDevices() {
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        pairedDevicesList.clear();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                DeviceListItem deviceItem = new DeviceListItem();
                deviceItem.setName(device.getName());
                deviceItem.setAddress(device.getAddress());
                deviceItem.setDevice(device);

                pairedDevicesList.add(deviceItem);
            }
            pairedEmpty.setVisibility(View.GONE);
        } else {
            pairedEmpty.setVisibility(View.VISIBLE);
        }
        pairedDevicesAdapter.notifyDataSetChanged();
        pairedSwipeRefresh.setRefreshing(false);
    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                DeviceListItem deviceItem = new DeviceListItem();
                deviceItem.setName(device.getName());
                deviceItem.setAddress(device.getAddress());
                deviceItem.setDevice(device);

                discoveredDevicesList.add(deviceItem);
                discoveredDevicesAdapter.notifyDataSetChanged();
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                findButton.setText(R.string.find_devices);
                discoveryProgress.setVisibility(View.GONE);
            }
        }
    };
}
