package com.hamdan.bluetoothremote;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Class for managing bluetooth connection threads
 */
public class BluetoothService {
    private static final String TAG = "BluetoothService";
    private static final String BT_UUID = "02645682-0000-1000-8000-00805f9b34fb";

    private static final int STATE_NONE = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    private int currentState;

    private Handler handler;
    private BluetoothAdapter bluetoothAdapter;

    private ConnectThread connectThread;
    private ConnectedThread connectedThread;

    BluetoothService(Handler handler) {
        this.handler = handler;
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        currentState = STATE_NONE;
    }

    /**
     * Attempts connection with remote device
     * @param device The remote bluetooth device
     */
    public synchronized void connect(BluetoothDevice device) {
        if (currentState == STATE_CONNECTING) {
            if (connectThread != null) {
                connectThread.cancel();
                connectThread = null;
            }
        }

        if (connectedThread != null) {
            connectedThread.cancel();
            connectedThread = null;
        }

        connectThread = new ConnectThread(device);
        connectThread.start();
        currentState = STATE_CONNECTING;
    }

    /**
     * Stops the connection
     */
    public synchronized void stop() {
        if (connectThread != null) {
            connectThread.cancel();
            connectThread = null;
        }
        if (connectedThread != null) {
            connectedThread.cancel();
            connectedThread = null;
        }

        currentState = STATE_NONE;
    }

    /**
     * Sends a command to the remote device
     * @param bytes information to be sent to the remote device
     */
    public synchronized void sendCommand(byte[] bytes) {
        if (currentState == STATE_CONNECTED) {
            connectedThread.write(bytes);
        }
    }

    /**
     * Sends a message to the handler that devices are connected
     */
    private void connectedResponse() {
        Message msg = handler.obtainMessage(Constants.MESSAGE_CONNECTED);
        handler.sendMessage(msg);
    }

    /**
     * Sends a message to the handler when a connection fails
     */
    private void connectionFailed() {
        Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Unable to connect device");
        bundle.putBoolean(Constants.CLOSE_ACTIVITY, true);
        msg.setData(bundle);
        handler.sendMessage(msg);

        currentState = STATE_NONE;
    }

    /**
     * Sends a message to the handler when a connection is lost
     */
    private void connectionLost() {
        Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Device connection was lost");
        bundle.putBoolean(Constants.CLOSE_ACTIVITY, true);
        msg.setData(bundle);
        handler.sendMessage(msg);

        currentState = STATE_NONE;
    }

    /**
     * Thread responsible for opening a connection with a remote device
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket socket;
        private final BluetoothDevice device;

        /**
         * Takes remote device and attempts to create a socket
         * @param device Remote bluetooth device
         */
        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;
            this.device = device;

            try {
                tmp = device.createRfcommSocketToServiceRecord(UUID.fromString(BT_UUID));
            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            socket = tmp;
        }

        /**
         * Creates a socket connection then calls the ConnectedThread for managing that socket
         */
        public void run() {
            bluetoothAdapter.cancelDiscovery();

            try {
                socket.connect();
            } catch (IOException connectException) {
                try {
                    socket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                connectionFailed();
                return;
            }

            connectedThread = new ConnectedThread(socket);
            connectedThread.start();
            currentState = STATE_CONNECTED;
            connectedResponse();
        }

        public void cancel() {
            try {
                socket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }

    /**
     * Thread responsible for managing input and output through the connected socket
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket socket;
        private final InputStream inputStream;
        private final OutputStream outputStream;
        private byte[] buffer;

        /**
         * Takes the connected socket and then attempts to open input and output streams for it
         * @param socket The socket connection with the remote device
         */
        public ConnectedThread(BluetoothSocket socket) {
            this.socket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating output stream", e);
            }

            inputStream = tmpIn;
            outputStream = tmpOut;
        }

        /**
         * Loops through the input stream to accept data from remote device
         */
        public void run() {
            buffer = new byte[1024];
            int numBytes;

            while (true) {
                try {
                    numBytes = inputStream.read(buffer);

                    Message readMsg = handler.obtainMessage(
                            Constants.MESSAGE_READ, numBytes, -1,
                            buffer);
                    readMsg.sendToTarget();
                } catch (IOException e) {
                    Log.d(TAG, "Input stream was disconnected", e);
                    connectionLost();
                    break;
                }
            }
        }

        /**
         * Sends a data over the socket connection to the remote device
         * @param bytes information to be sent to the remote device
         */
        public void write(byte[] bytes) {
            try {
                outputStream.write(bytes);

                Message writtenMsg = handler.obtainMessage(
                        Constants.MESSAGE_WRITE, -1, -1, buffer);
                writtenMsg.sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);

                Message writeErrorMsg =
                        handler.obtainMessage(Constants.MESSAGE_TOAST);
                Bundle bundle = new Bundle();
                bundle.putString("toast",
                        "Couldn't send data to the other device");
                writeErrorMsg.setData(bundle);
                handler.sendMessage(writeErrorMsg);
                currentState = STATE_NONE;
            }
        }

        public void cancel() {
            try {
                socket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }
}
