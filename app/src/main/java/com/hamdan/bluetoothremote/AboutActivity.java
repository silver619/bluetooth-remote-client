package com.hamdan.bluetoothremote;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Window;

/**
 * Simple activity that shows creator of the app
 */
public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
