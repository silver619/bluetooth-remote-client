package com.hamdan.bluetoothremote;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.hamdan.bluetoothremote.ui.control.LockableViewPager;
import com.hamdan.bluetoothremote.ui.control.OnControlFragmentInteractionListener;
import com.hamdan.bluetoothremote.ui.control.SectionsPagerAdapter;

import java.lang.ref.WeakReference;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Activity that holds the different control fragments for controlling the system with the server.
 * Uses a Tabbed Layout with various fragments for different control types (mouse, power, etc)
 */
public class ControlActivity extends AppCompatActivity implements OnControlFragmentInteractionListener {
    private static final String TAG = "ControlActivity";
    private View splashScreen;

    @DrawableRes
    private static final int[] TAB_ICONS = new int[] {
            R.drawable.ic_mouse_icon,
            R.drawable.ic_powerpoint_icon,
            R.drawable.ic_power_icon};

    BluetoothDevice device;

    BluetoothService bluetoothService;
    BluetoothHandler bluetoothHandler = new BluetoothHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        splashScreen = findViewById(R.id.splashScreen);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        LockableViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setSwipeable(false);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        for (int i = 0; i < TAB_ICONS.length; i++) {
            TabLayout.Tab tab = tabs.getTabAt(i);
            if(tab != null) {
                tab.setIcon(TAB_ICONS[i]);
            }
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            device = bundle.getParcelable(Constants.DEVICE_EXTRA);
        }

        bluetoothService = new BluetoothService(bluetoothHandler);

        if(device != null) {
            bluetoothService.connect(device);
        }
    }

    @Override
    public void onCommandSent(String command) {
        Log.d(TAG, "onCommandSent: " + command);
        bluetoothService.sendCommand(command.getBytes());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (bluetoothService != null) {
            bluetoothService.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bluetoothService != null) {
            bluetoothService.stop();
        }
    }

    // Handler for managing communication with the bluetooth connection threads
    protected static class BluetoothHandler extends Handler {
        WeakReference<ControlActivity> controlActivity;

        BluetoothHandler(ControlActivity controlActivity) {
            this.controlActivity = new WeakReference<>(controlActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case Constants.MESSAGE_READ:
                    byte[] buffer = (byte[]) msg.obj;
                    String cmd = new String(buffer);
                    Log.d(TAG, "handleMessage: "+ cmd);
                    break;
                case Constants.MESSAGE_TOAST:
                    Toast.makeText(controlActivity.get(), msg.getData().getString(Constants.TOAST), Toast.LENGTH_LONG).show();
                    if(msg.getData().getBoolean(Constants.CLOSE_ACTIVITY)) {
                        controlActivity.get().hideSplashScreen();
                        controlActivity.get().finish();
                    }
                    break;
                case Constants.MESSAGE_CONNECTED:
                    controlActivity.get().hideSplashScreen();
                    Log.d(TAG, "Devices connected");
                    break;
            }
        }
    }

    public void hideSplashScreen() {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(1000);

        fadeOut.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationEnd(Animation animation)
            {
                splashScreen.setVisibility(View.GONE);
            }
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {}
        });
        splashScreen.startAnimation(fadeOut);
    }
}