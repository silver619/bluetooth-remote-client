package com.hamdan.bluetoothremote.ui.control;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hamdan.bluetoothremote.Constants;
import com.hamdan.bluetoothremote.R;

/**
 * Fragment the holds the interface functionality for the powerpoint button controls
 */
public class MediaFragment extends Fragment {
    private OnControlFragmentInteractionListener listener;

    public MediaFragment() {
        // Required empty public constructor
    }

    public static MediaFragment newInstance() {
        MediaFragment fragment = new MediaFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View fragmentView = inflater.inflate(R.layout.fragment_media, container, false);

        fragmentView.findViewById(R.id.backwardButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(Constants.BACKWARD_COMMAND);
            }
        });

        fragmentView.findViewById(R.id.stopButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(Constants.STOP_COMMAND);
            }
        });

        fragmentView.findViewById(R.id.forwardButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(Constants.FORWARD_COMMAND);
            }
        });

        fragmentView.findViewById(R.id.restartSlidesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(Constants.RESTART_SLIDESHOW_COMMAND);
            }
        });

        fragmentView.findViewById(R.id.quitButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(Constants.QUIT_SLIDESOW_COMMAND);
            }
        });

        return fragmentView;
    }

    public void onButtonPressed(String command) {
        if (listener != null) {
            listener.onCommandSent(command);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnControlFragmentInteractionListener) {
            listener = (OnControlFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnControlFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
