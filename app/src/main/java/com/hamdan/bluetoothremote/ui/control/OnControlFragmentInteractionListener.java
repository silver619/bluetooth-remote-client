package com.hamdan.bluetoothremote.ui.control;

/**
 * Listener interface for communication between fragment and ControlActivity
 */
public interface OnControlFragmentInteractionListener {
    void onCommandSent(String command);
}
