package com.hamdan.bluetoothremote.ui.control;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.hamdan.bluetoothremote.Constants;
import com.hamdan.bluetoothremote.R;

/**
 * Fragment the holds the interface functionality for the mouse and keyboard controls
 */
public class MouseFragment extends Fragment {
    private boolean isMouseMovement = false;

    private OnControlFragmentInteractionListener listener;

    private EditText keyboardEditText;
    private TextWatcher keybaordTextWatcher;

    //used for mouse movement
    private float initialX = 0;
    private float initialY = 0;

    public MouseFragment() {
        // Required empty public constructor
    }

    public static MouseFragment newInstance() {
        MouseFragment fragment = new MouseFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_mouse, container, false);

        fragmentView.findViewById(R.id.leftClickButton).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    onButtonPressed(Constants.LEFT_CLICK_DOWN_COMMAND);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onButtonPressed(Constants.LEFT_CLICK_UP_COMMAND);
                }
                return true;
            }
        });

        fragmentView.findViewById(R.id.rightClickButton).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    onButtonPressed(Constants.RIGHT_CLICK_DOWN_COMMAND);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    onButtonPressed(Constants.RIGHT_CLICK_UP_COMMAND);
                }
                return true;
            }
        });

        fragmentView.findViewById(R.id.padView).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    initialX = motionEvent.getX();
                    initialY = motionEvent.getY();
                    isMouseMovement = false;
                } else if(motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    onMouseMovement(motionEvent);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if(!isMouseMovement) {
                        onButtonPressed(Constants.SINGLE_LEFT_CLICK_COMMAND);
                    }
                }
                return true;
            }
        });

        keyboardEditText = fragmentView.findViewById(R.id.keyboardEditText);

        // Capture backspaces in order to send them as a command
        keyboardEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    onButtonPressed(Constants.BACKSPACE_COMMAND);
                }
                return false;
            }
        });

        /*
         * A watcher that will send the typed character as a command as well as clear the field and
         * keep it empty as the user types
         */
        keybaordTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                if(charSequence.length() != 0) {
                    onButtonPressed(Constants.KEYBOARD_COMMAND_PREFIX + charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                keyboardEditText.removeTextChangedListener(keybaordTextWatcher);
                keyboardEditText.setText("");
                keyboardEditText.addTextChangedListener(keybaordTextWatcher);
            }
        };

        keyboardEditText.addTextChangedListener(keybaordTextWatcher);

        return fragmentView;
    }

    private void onButtonPressed(String command) {
        if (listener != null) {
            listener.onCommandSent(command);
        }
    }

    /**
     * Takes the pad view Movement Event and gets a movement delta to send to server
     * @param motionEvent the motion even form the TouchListener
     */
    private void onMouseMovement(MotionEvent motionEvent) {
        float currentX = motionEvent.getX() - initialX;
        float currentY = motionEvent.getY() - initialY;

        initialX = motionEvent.getX();
        initialY = motionEvent.getY();

        if (currentX != 0 && currentY != 0) {
            onButtonPressed("" + currentX + "," + currentY);
            float minNegMovement = -0.2f;
            float minPosMovement = 0.2f;
            if(currentX > minPosMovement ||
                    currentY > minPosMovement ||
                    currentX < minNegMovement ||
                    currentY < minNegMovement) {
                isMouseMovement = true;;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnControlFragmentInteractionListener) {
            listener = (OnControlFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnControlFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
