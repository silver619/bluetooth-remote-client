package com.hamdan.bluetoothremote.ui.devices;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hamdan.bluetoothremote.R;

import java.util.ArrayList;

/**
 * Custom adapter for device lists
 */
public class DevicesListAdapter extends BaseAdapter {
    private ArrayList<DeviceListItem> deviceList;
    private LayoutInflater layoutInflater;

    public DevicesListAdapter(Context context, ArrayList<DeviceListItem> deviceList) {
        this.deviceList = deviceList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return deviceList.size();
    }

    @Override
    public Object getItem(int i) {
        return deviceList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.devices_listview, viewGroup, false);
            holder = new ViewHolder();
            holder.name = view.findViewById(R.id.firstLine);
            holder.address = view.findViewById(R.id.secondLine);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.name.setText(deviceList.get(i).getName());
        holder.address.setText(deviceList.get(i).getAddress());
        return view;
    }

    static class ViewHolder {
        TextView name;
        TextView address;
    }
}
