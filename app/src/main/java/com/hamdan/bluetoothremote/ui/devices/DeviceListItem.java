package com.hamdan.bluetoothremote.ui.devices;

import android.bluetooth.BluetoothDevice;

/**
 * Class that holds data that is used in device lists
 */
public class DeviceListItem {
    private String name;
    private String address;
    private BluetoothDevice device;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public void setDevice(BluetoothDevice device) {
        this.device = device;
    }
}
