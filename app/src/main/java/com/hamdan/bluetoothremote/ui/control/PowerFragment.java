package com.hamdan.bluetoothremote.ui.control;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hamdan.bluetoothremote.Constants;
import com.hamdan.bluetoothremote.R;

/**
 * Fragment the holds the interface functionality for the power button controls
 */
public class PowerFragment extends Fragment {
    private OnControlFragmentInteractionListener listener;

    public PowerFragment() {
        // Required empty public constructor
    }

    public static PowerFragment newInstance() {
        PowerFragment fragment = new PowerFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View fragmentView = inflater.inflate(R.layout.fragment_power, container, false);

        fragmentView.findViewById(R.id.powerButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(Constants.POWER_COMMAND);
            }
        });

        fragmentView.findViewById(R.id.restartButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(Constants.RESTART_COMMAND);
            }
        });

        fragmentView.findViewById(R.id.logoutButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(Constants.LOGOUT_COMMAND);
            }
        });

        fragmentView.findViewById(R.id.hibernateButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(Constants.HIBERNATE_COMMAND);
            }
        });

        return fragmentView;
    }

    public void onButtonPressed(String command) {
        if (listener != null) {
            listener.onCommandSent(command);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnControlFragmentInteractionListener) {
            listener = (OnControlFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnControlFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
